#pragma once

namespace Tmpl8 {

class Ray;

class Renderer {
private:
	// For keeping track of the scene and rendering shit
	Camera *camera;
	BVH *bvh;

	std::vector<Triangle *> scene;
	std::vector<Triangle *> lights;

	// For RNG
	std::mt19937 rng;
	std::uniform_int_distribution<uint> distUInt;
	std::uniform_real_distribution<float> distFloat;

	// Whether to display debug information
	bool showDebug = false;

	// The maximum number of times a ray can spawn another ray
	static const int MAXIMUM_SPAWN_DEPTH = 10;

public:
	// Debug info...
	unsigned long long sumOfAllColors;

	// Number of samples per pixel to use for the path tracer
	const uint SPP = 1;

	Renderer(Camera *camera, BVH *bvh, std::vector<Triangle *> &scene, std::vector<Triangle *> &lights)
	: camera(camera), scene(scene), lights(lights), bvh(bvh), sumOfAllColors(0) {

		// Setup RNG
		rng.seed(std::random_device()());
		setRandIntDistribution();
		setRandFloatDistribution();
	}

	// Renders the entire scene to the screen, with optional debug crap, and
	// also can be used to let the path tracing converge if necessary, by setting
	// frameNr to > 0. A value of 0 means its a new frame.
	void renderScene(Surface *screen, bool displayDebug, uint frameNr, vec3 *accumulators);

	// Returns True/False if the given ray intersected something
	inline bool intersects(Ray &ray, float minT = 0.0f, float maxT = Ray::MAX_T) {
		return ray.t > minT && ray.t < maxT;
	}


	uint findSceneIntersection(Ray &ray, float *u, float *v, bool doEarlyOut = false, float minT = 0.0f, float maxT = Ray::MAX_T);
	vec3 getReflectionColor(Ray &ray, float3 position, float3 normal, int depth);
	vec3 traceRay(Ray &ray, int depth = MAXIMUM_SPAWN_DEPTH);
	vec3 directIllumination(Ray &ray, float3 position, float3 normal);


	// Used for handling floating point inaccuracies when dealing with
	// intersections of shadow rays etc
	static constexpr float SHADOW_EPSILON = 0.001f;


	// Resets the uniform int distribution based on the number of lights
	inline void setRandIntDistribution() {
		distUInt = std::uniform_int_distribution<uint>(0, lights.size() - 1);
	}

	// Resets the uniform float distribution
	inline void setRandFloatDistribution(float min = 0.0f, float max = 1.0f) {
		distFloat = std::uniform_real_distribution<float>(min, max);
	}

	// Returns a random integer number in the range [min, max] inclusive
	inline uint getRandUInt() {
		return distUInt(rng);
	}

	// Returns a random float number in the range [min, max] inclusive
	inline float getRandFloat() {
		return distFloat(rng);
	}
};

};
