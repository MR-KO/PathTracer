#include "template.h"

namespace Tmpl8 {

// Separate rand manager code...
std::mt19937 GeometryRandRng;
std::uniform_real_distribution<float> GeometryRandDistFloatUniform;
std::normal_distribution<float> GeometryRandDistFloatNormal;

// Returns a random number using the given RNG stuff
inline float getRandUniform() {
	return GeometryRandDistFloatUniform(GeometryRandRng);
}

// Returns a random number using the given Gaussian random stuff
inline float getRandNormal() {
	return GeometryRandDistFloatNormal(GeometryRandRng);
}

// Setup RNG
void setupGeometryRand() {
	GeometryRandRng.seed(std::random_device()());
	GeometryRandDistFloatUniform = std::uniform_real_distribution<float>(0.0f, 1.0f);
	GeometryRandDistFloatNormal = std::normal_distribution<float>(0.0f, 1.0f);
}



float3 Triangle::getNormal(float3 position, float alfa, float beta) {
	// Use barycentric coordinates to interpolate the normals
	float gamma = getGamma(alfa, beta);

	// Check barycentric coordinates: 0 <= u, v <= 1
	assert(alfa >= 0.0f);
	assert(beta >= 0.0f);
	assert(gamma >= 0.0f);	// u + v <= 1, so now 0 <= u <= 1 and 0 <= v <= 1

	// Alternative version... shouldnt be necessary...
	// return (normals[0] * alfa + normals[1] * beta + normals[2] * gamma).normalized();

	// Return interpolated normal
	return normals[0] * alfa + normals[1] * beta + normals[2] * gamma;
}


float3 Triangle::getRandomPointOnSurface(float *u, float *v) {
	// Get a random values for alfa, and then another random value to determine
	// the "scaling" between the remainder for beta and gamma
	float alfa = *u = getRandUniform();
	float beta = *v = getRandUniform() * (1.0f - alfa);
	float gamma = getGamma(alfa, beta);
	return vertices[0] * alfa + vertices[1] * beta + vertices[2] * gamma;
}


float Triangle::calcSurfaceArea() {
	// Uses Heron's formula to easily calculate this, see wikipedia
	float a = (vertices[1] - vertices[2]).sqrLength();
	float b = (vertices[0] - vertices[2]).sqrLength();
	float c = (vertices[0] - vertices[1]).sqrLength();
	return 0.25f * sqrtf(4.0f * a * b - (a + b - c) * (a + b - c));
}



bool Triangle::intersect(Ray &ray, float *u, float *v) {
	float3 edge1 = vertices[1] - vertices[0];
	float3 edge2 = vertices[2] - vertices[0];
	float3 p = ray.direction.cross(edge2);
	float det = edge1.dot(p);

	if (det > - Triangle::INTERSECTION_EPSILON && det < Triangle::INTERSECTION_EPSILON) {
		return false;	// No intersection
	}

	float inverseDet = 1.0f / det;
	float3 temp = ray.origin - vertices[0];
	*u = temp.dot(p) * inverseDet;

	if (*u < 0.0f || *u > 1.0f) {
		return false;	// No intersection
	}

	float3 q = temp.cross(edge1);
	*v = ray.direction.dot(q) * inverseDet;

	if (*v < 0.0f || *u + *v > 1.0f) {
		return false;	// No intersection
	}

	ray.t = edge2.dot(q) * inverseDet;
	return true;
}

};
