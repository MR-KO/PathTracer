#include "template.h"

namespace Tmpl8 {

// Dafuq C++
constexpr float Renderer::SHADOW_EPSILON;

void Renderer::renderScene(Surface *screen, bool displayDebug, uint frameNr, vec3 *accumulators) {
	// Get our rays and trace them
	std::vector<Ray> &rays = camera->generateRays();

	// TODO: Use anti-aliasing?
	sumOfAllColors = 0;
	Pixel *buffer = screen->GetBuffer();
	uint x = 0, y = 0;

	for (uint i = 0; i < rays.size(); ++i) {
		vec3 pixelColor = traceRay(rays[i], Renderer::MAXIMUM_SPAWN_DEPTH);
		Pixel pixel = FloatPixelToColor(pixelColor);
		sumOfAllColors += pixel;
		screen->Plot(x, y, pixel);

		if (++x % SCRWIDTH == 0) {
			x = 0;
			++y;
		}
	}

	// Display optional debug info
	// if (displayDebug) { // Do shit later... }
}


vec3 Renderer::traceRay(Ray &ray, int depth) {
	// Check recursion depth...
	if (depth == 0) {
		printf("Maximum recursion depth reached!\n");
		return vec3(0.0f, 0.0f, 0.0f);
	}

	// For bary-centric coordinates;
	float u = 0.0, v = 0.0;

	// Check intersection of this ray with the scene
	uint sceneIndex = findSceneIntersection(ray, &u, &v);

	if (intersects(ray)) {
		// Intersection!
		float3 position = ray.getPosition();
		float3 normal = scene[sceneIndex]->getNormal(position, u, v);
		Material &material = scene[sceneIndex]->material;
		vec3 matColor = material.getColor(u, v);

		switch (material.type) {
			case MATERIAL_TYPE::MATERIAL_DIFFUSE:
				return matColor * directIllumination(ray, position, normal);

			case MATERIAL_TYPE::MATERIAL_MIRROR:
				return matColor * getReflectionColor(ray, position, normal, depth);

			case MATERIAL_TYPE::MATERIAL_DIFFUSE_AND_MIRROR: {
				float diffuse = material.diffuse;
				return matColor * (diffuse * directIllumination(ray, position, normal) +
					(1.0f - diffuse) * getReflectionColor(ray, position, normal, depth));
			}

			case MATERIAL_TYPE::MATERIAL_GLASS:
				printf("ERMAHGERD, GLERSS!\n");
				return matColor;

			case MATERIAL_TYPE::MATERIAL_LIGHT:
				printf("This shouldnt happen!\n");
				return matColor;

			default:
				printf("WTF?\n");
				return matColor;
		}
	}

	return vec3(0.0f, 0.0f, 0.0f);
}


uint Renderer::findSceneIntersection(Ray &ray, float *u, float *v, bool doEarlyOut, float minT, float maxT) {
	uint sceneIndex = scene.size() + 42;
	float closestT = maxT;
	float uTemp, vTemp;

	for (uint i = 0; i < scene.size(); ++i) {
		bool intersects = scene[i]->intersect(ray, &uTemp, &vTemp);

		// Keep track of closest, valid t value
		if (intersects && ray.t > minT && ray.t < closestT) {
			sceneIndex = i;
			closestT = ray.t;
			*u = uTemp;
			*v = vTemp;

			if (doEarlyOut) {
				return sceneIndex;
			}
		}
	}

	// Reset ray.t to closest t found (may be maxT, so maybe no intersection found!)
	ray.t = closestT;
	return sceneIndex;
}


vec3 Renderer::directIllumination(Ray &ray, float3 position, float3 normal) {
	// Re-use (light) ray and keep color accumulator
	vec3 color(0.0f, 0.0f, 0.0f);
	float u = 0.0f, v = 0.0f;

	// Add light sources contribution
	for (uint i = 0; i < lights.size(); ++i) {
		// Get direction to light source
		float3 direction = lights[i]->center - position;
		ray.setDirection(direction);

		// We add a little bit of the normal to the origin of the light ray
		// to prevent self-intersection with the already intersected object.
		ray.origin = position + normal * Ray::EPSILON_T;
		ray.t = 0.0f;

		// Check if the ray intersects with non-lights...
		uint sceneIndex = findSceneIntersection(ray, &u, &v, true);

		if (!intersects(ray)) {
			// Check dot product between light and ray directoin (aka, is the
			// light visible from this side?)
			float dotProduct = normal.dot(ray.direction);

			if (dotProduct > 0.0f) {
				// Light source is not occluded, add light contribution
				float distance = direction.length();
				vec3 lightContribution = lights[i]->material.getColor(u, v) * dotProduct / distance;
				// printf("Light contribution = %.2f, %.2f, %.2f\n", lightContribution.x,
				// 	lightContribution.y, lightContribution.z);
				color += lightContribution;
			}
		}
	}

	return color;
}


vec3 Renderer::getReflectionColor(Ray &ray, float3 position, float3 normal, int depth) {
	// Re-use ray object, don't need another one
	float3 reflected = getReflectedVector(ray.direction, normal);
	ray.origin = position + reflected * Ray::EPSILON_T;
	ray.setDirection(reflected);
	ray.t = 0.0f;
	return traceRay(ray, depth - 1);
}

};
