#include "template.h"

namespace Tmpl8 {

vec3 Material::getColor(float u, float v) {
	// For lights we return the intensity * lightcolor directly
	if (type == MATERIAL_TYPE::MATERIAL_LIGHT) {
		return color * diffuse;
	}

	// If no texture for this material, or invalid coords, return normal color
	if (texture == NULL || u > 1.0f || u < 0.0f || v < 0.0f || v > 1.0f) {
		return color;
	}

	// Get texture details
	int width = texture->GetWidth();
	int height = texture->GetHeight();
	Pixel* buffer = texture->GetBuffer();

	// u = alfa, v = beta, w = gamma
	float w = getGamma(u, v);

	// Calculate texture coordinates
	float2 tex = u * texCoords[0] + v * texCoords[1] + w * texCoords[2];

	// Old, nearest neighbour interpolation below
	// int x = static_cast<int>(width * tex.x);
	// int y = static_cast<int>(height * tex.y);

	// Bi-linear interpolation of the texture, get scales and stuff
	float xTemp = width * tex.x;
	float yTemp = height * tex.y;
	float xCeil = ceil(xTemp);
	float yCeil = ceil(yTemp);
	float xScale = xTemp - static_cast<long>(xTemp);
	float yScale = yTemp - static_cast<long>(yTemp);

	// Get the 4 pixel coordinates
	int x1 = static_cast<int>(floor(xTemp));
	int x2 = static_cast<int>(xCeil);
	int y1 = static_cast<int>(floor(yTemp));
	int y2 = static_cast<int>(yCeil);

	// Get colors at those pixels
	vec3 colorx1y1 = PixelToFloatColor(buffer[y1 * width + x1]);
	vec3 colorx2y1 = PixelToFloatColor(buffer[y1 * width + x2]);
	vec3 colorx1y2 = PixelToFloatColor(buffer[y2 * width + x1]);
	vec3 colorx2y2 = PixelToFloatColor(buffer[y2 * width + x2]);

	// And do finial bi-linear interpolation
	vec3 color = (1.0f - xScale) * (1.0f - yScale) * colorx1y1 +
		(1.0f - yScale) * xScale * colorx2y1 + (1.0f - xScale) * yScale * colorx1y2 +
		xScale * yScale * colorx2y2;
	color.x = max(color.x, 0.0f);
	color.y = max(color.y, 0.0f);
	color.z = max(color.z, 0.0f);

	// Return the texture color to vec3
	// Pixel temp = buffer[y * width + x];
	// vec3 color = PixelToFloatColor(temp);

	// printf("At u = %f, v = %f, w = %f, tex = (%f, %f), x = %d, y = %d, color = (%f, %f, %f)\n",
	// 	u, v, w, tex.x, tex.y, x, y, color.x, color.y, color.z);

	return color;
}



TextureCache::~TextureCache() {
	// Delete allocated textures, C++11 style
	for (auto &keypair : texCache) {
		delete keypair.second;
	}
}


// For easy reference in load and get functions
typedef std::unordered_map<std::string, Surface*>::const_iterator constIterator;


void TextureCache::load(std::string fileName) {
	// Check if its not in the cache already, otherwise skip it
	constIterator found = texCache.find(fileName);

	if (found == texCache.end()) {
		// Not found, add it to the cache
		texCache[fileName] = new Surface(fileName.c_str());
	}
}

Surface* TextureCache::get(std::string fileName) {
	// Check if its in the cache, otherwise return NULL
	constIterator found = texCache.find(fileName);

	if (found == texCache.end()) {
		return NULL;
	} else {
		return found->second;
	}
}

};
