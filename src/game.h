#pragma once

#define SCRWIDTH	600
#define SCRHEIGHT	400

namespace Tmpl8 {

class Surface;
class Camera;
class Renderer;

class Game {
public:
	void SetTarget( Surface* _Surface ) { screen = _Surface; }
	void Init();
	void Shutdown();
	void HandleInput( float dt );
	void Tick( float dt );
	void MouseUp( int _Button );
	void MouseDown( int _Button );
	void MouseMove( int _X, int _Y );
	void KeyUp( int a_Key );
	void KeyDown( int a_Key );
	void SetupBVHRenderer();
	void SetupCrappyScene();
	void SetupBasicScene();
	void SetupTestScene();
	void Reset();
	void ShowDetails(float dt);

	// Constants for key movement (aka, moving around)
	static const int KEY_W = 26;
	static const int KEY_A = 4;
	static const int KEY_S = 22;
	static const int KEY_D = 7;
	static const int KEY_SPACE = 44;
	static const int KEY_C = 6;
	static const int KEY_LSHIFT = 225;
	static const int KEY_R = 21;

	// Used for looking around with the mouse
	static const int LEFT_MOUSE_BUTTON = 1;

	// Used for controlling movement speed
	static const int KEY_DECREASE = 45;	// key -_ (right of the ) key)
	static const int KEY_INCREASE = 46; // key =+ (left of the backspace key)
	static constexpr float CAMERA_MOVEMENT_CHANGE = 1.0f;

	// Used for handling scene loading
	static const int KEY_1 = 30;
	static const int KEY_2 = 31;
	static const int KEY_3 = 32;

	// For debugging, e.g. displaying fps, etc
	static const int KEY_DEBUG = 12;	// key i
	bool displayDebug = false;

private:
	Surface *screen;
	Renderer *renderer;
	TextureCache *texCache;

	// For keeping track of accumulated pixel colors for the path tracer
	vec3 *accumulators;

	// For keeping track of the scene, lights etc
	std::vector<Triangle *> scene;
	std::vector<Triangle *> lights;	// lights are now pointers to objects in scene for easy access
	Camera *camera;
	BVH *bvh;

	// For keeping track of movement
	std::unordered_set<int> keysActive;
	bool sceneNeedsUpdate = true;
	bool firstTick = true;

	// For keeping track of mouse movement
	bool lookAroundActive = false;
	bool firstTime = true;
	int previousX = 0, previousY = 0;

	// For keeping track of the frame numbers to use for converging path tracer
	uint frameNr = 0;
	float convergingTime = 0.0f;
	uint numSamplesUsed = 0;

	// For showing the colors on the screen where the mouse is pointing
	int showColorX = -1;
	int showColorY = -1;
};

}; // namespace Tmpl8
