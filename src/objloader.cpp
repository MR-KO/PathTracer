#include "template.h"

#define TINYOBJLOADER_IMPLEMENTATION

#include "tiny_obj_loader.h"
#include "objloader.h"

namespace Tmpl8 {

Material ObjLoader::convertObjMaterial(std::vector<tinyobj::material_t> &materials,
	int materialId, std::string materialDir, TextureCache *texCache, bool hasTexture) {

	// TODO: Figure out what to do with the ambient and  other parts...
	// printf("materialDir = %s, materialId = %d\n", materialDir.c_str(), materialId);

	// Get the diffuse color and texture
	vec3 color;
	std::string textureFileName = "";

	if (materials.size() == 0 || materialId == -1) {
		color = vec3(1.0f, 1.0f, 1.0f);		// Default color
	} else {
		color = vec3(materials[materialId].diffuse[0],
			materials[materialId].diffuse[1], materials[materialId].diffuse[2]);

		// And load texture into the cache... if there is one
		if (hasTexture) {
			textureFileName = materialDir + materials[materialId].diffuse_texname;
			texCache->load(textureFileName);
		}
	}

	// printf("Color = (%f, %f, %f)\n", color.x, color.y, color.z);

	// Also.. TODO: Handle the other options and upgrade Material class....
	Material material(MATERIAL_TYPE::MATERIAL_DIFFUSE, color);

	if (hasTexture) {
		material.setTexture(texCache->get(textureFileName));
	}

	return material;
}

// Swaps 2 elements a and b in the array
inline void swapElements(float2 array[], int a, int b) {
	float2 temp = array[a];
	array[a] = array[b];
	array[b] = temp;
}

void ObjLoader::loadIntoScene(std::vector<Triangle *> &scene,
	std::string &objectFile, std::string &materialDir, TextureCache *texCache) {
	// Prepare crap for loading object file
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	// Try to load the object
	printf("Loading object from %s...\n", objectFile.c_str());
	const char *dir = (materialDir == "") ? NULL : materialDir.c_str();
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, objectFile.c_str(), dir);

	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
	}

	if (!ret) {
		return;
	}

	printf("# of shapes    : %lu\n", shapes.size());
	printf("# of materials : %lu\n", materials.size());
	printf("# of normals   : %lu\n", attrib.normals.size());

	// Determine if the object has textures
	bool hasTextures = attrib.texcoords.size() != 0;

	// Loop over shapes
	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;

		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];

			// Check if we load something other than triangles
			if (fv != 3) {
				printf("Loading a non-triangle with fv = %d... Nope, not gonna happen!\n", fv);
				return;
			}

			// Create new vertices and normals for a triangle, assuming fv == 3
			// We need 4 vertices and 3 normals, since the first vertex is the
			// center of the triangle
			float3 vertices[4] = {float3(0, 0, 0), float3(0, 0, 0), float3(0, 0, 0), float3(0, 0, 0)};
			float3 normals[3] = {float3(0, 0, 0), float3(0, 0, 0), float3(0, 0, 0)};
			float2 texCoords[3] = {float2(0, 0), float2(0, 0), float2(0, 0)};

			// printf("index_offset = %lu, fv = %d\n", index_offset, fv);

			// Some object files are assholes and don't include normals...
			bool calcNormalManually = false;

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				// Load the vertex for this face
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				int ownIndex = fv - v;
				// printf("\townIndex = %d\n", ownIndex);
				vertices[ownIndex].x = attrib.vertices[3 * idx.vertex_index + 0];
				vertices[ownIndex].y = attrib.vertices[3 * idx.vertex_index + 1];
				vertices[ownIndex].z = attrib.vertices[3 * idx.vertex_index + 2];
				// printf("Loaded vertex (%f, %f, %f)\n",
				// 	vertices[ownIndex].x, vertices[ownIndex].y, vertices[ownIndex].z);

				// Decrement ownindex since we can skip the first element for
				// the center position
				ownIndex--;

				// Check if the vertex has a normal (WTF?)
				if (attrib.normals.size() != 0) {
					normals[ownIndex].x = attrib.normals[3 * idx.normal_index + 0];
					normals[ownIndex].y = attrib.normals[3 * idx.normal_index + 1];
					normals[ownIndex].z = attrib.normals[3 * idx.normal_index + 2];
					// printf("Loaded normal (%f, %f, %f)\n",
					// 	normals[ownIndex].x, normals[ownIndex].y, normals[ownIndex].z);
				} else {
					// We are now loading a vertex without normal...
					printf("Are you fucking serious right now? A vertex with no normal???\n");
					calcNormalManually = true;
				}

				// Check if the vertex actually has texture coordinates, and get them
				if (hasTextures) {
					texCoords[ownIndex].x = attrib.texcoords[2 * idx.texcoord_index + 0];
					texCoords[ownIndex].y = attrib.texcoords[2 * idx.texcoord_index + 1];
					// printf("Got tex coords (%f, %f) with idx.texcoord_index = %d\n",
					// 	texCoords[ownIndex].x, texCoords[ownIndex].y, idx.texcoord_index);
				}
			}

			// Calculate center vertex of the triangle
			vertices[0] = (vertices[1] + vertices[2] + vertices[3]) / 3.0f;
			index_offset += fv;

			if (calcNormalManually) {
				// Omg, you lazy faggot, just include normals for fucks sake...
				// Now I dont give a shit about normal direction either
				float3 edge1 = vertices[1] - vertices[0];
				float3 edge2 = vertices[2] - vertices[1];
				float3 normal = edge1.cross(edge2);

				for (uint i = 0; i < 3; ++i) {
					normals[i] = normal;
				}
			}

			// Per-face material, convert it to our format
			int materialId = shapes[s].mesh.material_ids[f];
			Material material = convertObjMaterial(materials, materialId,
				materialDir, texCache, hasTextures);

			// And add texture coordinates, if there were any...
			if (hasTextures) {
				// But first we swap some values, because of my shitty code,
				// aka "rotate" the texture coords array by 1 element:
				// [a, b, c] -> [b, c, a]...
				swapElements(texCoords, 0, 2);
				swapElements(texCoords, 1, 0);
				material.setTexCoords(texCoords);
			}

			// Create the new triangle and add it to the scene
			scene.push_back(new Triangle(vertices, normals, material));
		}
	}

	// Print material shits..
	for (size_t i = 0; i < materials.size(); i++) {
		printf("material[%ld].name = %s\n", i, materials[i].name.c_str());
		printf("  material.Ka = (%f, %f, %f)\n", materials[i].ambient[0], materials[i].ambient[1], materials[i].ambient[2]);
		printf("  material.Kd = (%f, %f, %f)\n", materials[i].diffuse[0], materials[i].diffuse[1], materials[i].diffuse[2]);
		printf("  material.Ks = (%f, %f, %f)\n", materials[i].specular[0], materials[i].specular[1], materials[i].specular[2]);
		printf("  material.Tr = (%f, %f, %f)\n", materials[i].transmittance[0], materials[i].transmittance[1], materials[i].transmittance[2]);
		printf("  material.Ke = (%f, %f, %f)\n", materials[i].emission[0], materials[i].emission[1], materials[i].emission[2]);
		printf("  material.Ns = %f\n", materials[i].shininess);
		printf("  material.Ni = %f\n", materials[i].ior);
		printf("  material.dissolve = %f\n", materials[i].dissolve);
		printf("  material.illum = %d\n", materials[i].illum);
		printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
		printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
		printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
		printf("  material.map_Ns = %s\n", materials[i].specular_highlight_texname.c_str());
		std::map<std::string, std::string>::const_iterator it(materials[i].unknown_parameter.begin());
		std::map<std::string, std::string>::const_iterator itEnd(materials[i].unknown_parameter.end());

		for (; it != itEnd; it++) {
			printf("  unknown material.%s = %s\n", it->first.c_str(), it->second.c_str());
		}

		printf("\n");
	}

	printf("Object loading done!\n");
}

};
