#pragma once

namespace Tmpl8 {

void setupGeometryRand();

// Helper function to ensure valid gamma value
inline float getGamma(const float alfa, const float beta) {
	float gamma = 1.0f - alfa - beta;

	// Adjust gamma to zero if it is close to zero, for floating point inaccuracy
	if (abs(gamma) <= 0.000001f) {
		gamma = 0.0f;
	}

	return gamma;
}


class Triangle {
public:
	float3 center;		// Center will be calculated automatically.
	float3 normals[3];	// Triangle normals point away from the Triangle
	float3 vertices[3];
	float surfaceArea;
	Material material;

	// The given vertices array should hold 3 vertices, and each of those
	// 3 vertices has its own normal... The real normal will be linearly
	// interpolated between them.
	Triangle(float3 triangleVertices[3], float3 vertexNormals[3], Material material)
	: center(getCenter(triangleVertices)), material(material) {

		// Load vertices and normals into triangle
		for (int i = 0; i < 3; ++i) {
			vertices[i] = triangleVertices[i];
			normals[i] = vertexNormals[i];
			normals[i].normalize();
		}

		// Calculate and set surface area
		surfaceArea = calcSurfaceArea();
	}

	/** Returns true/flase indicating whether this object is a light source */
	inline bool isLightSource() {
		return material.type == MATERIAL_TYPE::MATERIAL_LIGHT;
	}


	// Used for handling floating point inaccuracies when dealing with
	// intersections, as well as with barycentric coordinates
	static constexpr float INTERSECTION_EPSILON = 0.000001f;


	/**
		Returns true/false if the given ray intersects the geometry/object.
		Ray.t will be set for which t value the ray "intersects" the object.
		If this value t is in the range [0, Ray::MAX_T], then it intersects
		the object. Otherwise, it does not intersect the object. If multiple
		intersections are found, the closest/first one will be returned (from
		the perspective of the ray origin). The float pointers to u and v are used
		for calculating the barycentric coordinates of the intersection point.
	*/
	bool intersect(Ray &ray, float *u = NULL, float *v = NULL);

	/**
		Returns the normal vector at the given position, with optionally the
		given barycentric coordinates encoded in the u and v float pointers.
		See intersect function above for more information on these coordinates.
	*/
	float3 getNormal(float3 position, float u, float v);

	/**
		Returns a random point on the (surface) of the geometry object. Can store
		the u and v parameters in the given u and v pointers if the method requires it.
	*/
	float3 getRandomPointOnSurface(float *u, float *v);

	/** Calculates and returns the surface area of this geometry object. */
	float calcSurfaceArea();

	/** Returns the surface area of this object. */
	inline float getSurfaceArea() {
		return surfaceArea;
	}
};

};
