#pragma once

namespace Tmpl8 {

enum class MATERIAL_TYPE {
	MATERIAL_DIFFUSE,
	MATERIAL_MIRROR,
	MATERIAL_DIFFUSE_AND_MIRROR,
	MATERIAL_GLASS,
	MATERIAL_LIGHT
};

class Surface;

class Material {
public:
	MATERIAL_TYPE type;
	vec3 color;				// Actual color of the material/light
	float diffuse;			// How diffuse the object is (if type != diffuse), or intensity if type == light
	float refractionIndex;	// The refraction index if you enter the material

	// Use the provided FreeImage to load textures (optionally)
	// Currently only supports textures for triangles
	Surface *texture;
	float2 texCoords[3];

	Material(MATERIAL_TYPE type, vec3 color, float diffuse = 1.0f)
	: Material(type, color, diffuse, 1.0f) { }

	Material(MATERIAL_TYPE type, vec3 color, float diffuse, float refraction)
	: type(type), color(color), diffuse(diffuse), texture(NULL), refractionIndex(refraction) {

		// Check diffuse value, must be at least 0.0f
		assert(diffuse >= 0.0f);

		// Set texCoords all to -1
		for (uint i = 0; i < 3; ++i) {
			texCoords[i] = float2(-1.0f, -1.0f);
		}

		texture = NULL;
	}

	// Copies over the new texture coordinates
	void setTexCoords(float2 newTexCoords[3]) {
		for (uint i = 0; i < 3; ++i) {
			texCoords[i] = newTexCoords[i];
		}
	}

	// Sets texture pointer to a real texture pointer
	void setTexture(Surface *newTexture) {
		texture = newTexture;
	}

	// Returns the color at the given barycentric coordinates. If u or v is
	// outside the range of [0.0f, 1.0f], or no texture is set, then the regular
	// object color will be used. Else the texture is linearly interpolated and
	// the resulting color is returned.
	vec3 getColor(float u = -1.0f, float v = -1.0f);
};



// This class is purely meant to be used as a singleton... Please do so...
class TextureCache {
private:
	// Texture cache maps a string fileName to a Surface* holding the texture
	std::unordered_map<std::string, Surface*> texCache;

public:
	TextureCache() { }
	~TextureCache();

	// Loads a texture into the cache, if it does not exist already
	void load(std::string fileName);

	// Returns a pointer to the texture in the cache, or NULL if not found
	Surface* get(std::string fileName);
};

};
