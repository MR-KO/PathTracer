#include "template.h"

namespace Tmpl8 {

void Camera::updateCameraPlane() {
	direction.normalize();
	center = position + direction * distance;

	// Get the vector orthogonal to the direction vector to get the left movement
	// We add {0, 0.1, 0} to the direction vector to avoid getting the zero vector
	// for the cross product
	float3 leftMove = (direction + float3(0, 0.1, 0)).cross(direction).normalized();

	// Get the vector orthogonal to direction and left, and scale it with upDistance
	float3 upMove = direction.cross(leftMove).normalized();

	// Renormalize the left vector again
	leftMove = upMove.cross(direction).normalized();

	// And recalculate the corners of the view plane
	p0 = center - leftMove + upMove;
	p1 = center + leftMove + upMove;
	p2 = center - leftMove - upMove;
	right = p1 - p0;
	up = p2 - p0;
}

void Camera::moveCamera(float forwardDistance, float leftDistance, float upDistance) {
	// Update the new position of the camera
	position = position + direction * forwardDistance -
		right.normalized() * leftDistance - up.normalized() * upDistance;
	updateCameraPlane();
}

void Camera::updateCameraDirection(int dx, int dy) {
	// Set the new direction vector... Use the corner vectors of the view plane
	// We do this by finding out to which pixel the new view direction points,
	// (which is the center pixel + [dx, dy], scaled to be in the [-1.0f, 1.0f]
	// range). Then we can update the new direction and camera plane again.
	float uStep = (screenWidth / 2 + dx) * uStepSize - 1.0f;
	float vStep = (screenHeight / 2 + dy) * vStepSize - 1.0f;
	direction = direction + right * ratioFov * aspectRatio * uStep + up * ratioFov * vStep;
	direction.normalize();
	updateCameraPlane();
}

std::vector<Ray>& Camera::generateRays() {
	// Initialize the rays
	float3 rayOrigin = position;
	unsigned int index = 0;

	for (uint y = 0; y < screenHeight; ++y) {
		// Make sure vStep is in the range [-1.0f, 1.0f]
		float vStep = y * vStepSize - 1.0f;

		for (uint x = 0; x < screenWidth; ++x) {
			// Determine the ray direction vector based on the x and y
			// coordinates using the stepsizes
			// uStep and vStep are now in the range [-1.0f, 1.0f]
			float uStep = x * uStepSize - 1.0f;
			float3 rayDirection = direction + right * ratioFov * aspectRatio * uStep + up * ratioFov * vStep;
			rayDirection.normalize();

			// First (re)setup the pre-allocated ray...
			rays[index].origin = rayOrigin;
			rays[index].direction = rayDirection;
			rays[index].invDirection = 1.0f / rayDirection;
			++index;
		}
	}

	return rays;
}

};
