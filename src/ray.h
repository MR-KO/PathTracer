#pragma once

namespace Tmpl8 {

class Geometry;
class LightSource;


class Ray {
public:
	float t;
	float3 origin;
	float3 direction;
	float3 invDirection;	// Used for faster AABB intersection tests

	// Ray = position + t * direction, this is the max value of t
	// It is close to the maximum value of a float (somewhere around 1e38f)
	static constexpr float MAX_T = 1e34f;

	// Epsilon value for ray shit
	static constexpr float EPSILON_T = 0.0001f;

	Ray() : Ray(float3(0.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 0.0f)) { }

	Ray(float3 origin, float3 initDirection) : t(0.0f), origin(origin) {
		setDirection(initDirection);
	}

	// Returns the position of the ray given a certain t > 0.0
	float3 getPosition();

	// Sets new direction
	void setDirection(float3 newDirection);
};

};
