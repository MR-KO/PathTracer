#pragma once

namespace Tmpl8 {

inline float3 getCenter(float3 vertices[3]) {
	return (vertices[0] + vertices[1] + vertices[2]) / 3.0f;
}

inline float3 getReflectedVector(float3 direction, float3 normal) {
	return direction - 2.0f * dot(direction, normal) * normal;
}

};
