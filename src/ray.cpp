#include "template.h"

namespace Tmpl8 {

constexpr float Ray::MAX_T;
constexpr float Ray::EPSILON_T;


float3 Ray::getPosition() {
	if (t > 0.0 && t < Ray::MAX_T) {
		return origin + direction * t;
	} else {
		float3 position = origin + direction * t;
		printf("Dafuq? t = %f, resulting point = (%f, %f, %f)\n", t, position.x, position.y, position.z);
		return position;
	}
}

void Ray::setDirection(float3 newDirection) {
	newDirection.normalize();
	direction = newDirection;
	invDirection = 1.0f / newDirection;
}

};
