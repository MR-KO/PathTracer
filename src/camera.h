#pragma once

namespace Tmpl8 {

class Surface;

class Camera {
public:
	float3 position;
	float3 direction;
	float distance;
	float aspectRatio;
	float ratioFov;
	float uStepSize, vStepSize;
	float sensitivity;
	float movementSpeed;	// in game units/s

	float3 center;
	float3 p0, p1, p2, up, right;

	unsigned int screenWidth, screenHeight;

	// For pre-allocating the rays
	std::vector<Ray> rays;

	Camera(float3 position, float3 direction, float distance, float fovYDegrees,
	unsigned int width, unsigned int height, float sensitivity = 1.0f)
	: position(position), direction(direction), distance(distance),
	screenWidth(width), screenHeight(height), sensitivity(sensitivity) {

		assert(distance > 0.0);
		updateCameraPlane();
		movementSpeed = 5.0f;

		// For fixing aspect ratio shit
		// Method taken from from http://stackoverflow.com/questions/12892906/generate-a-vector/12892966#12892966
		aspectRatio = (float)screenWidth / screenHeight;
		float fovY = deg2rad(fovYDegrees) / 2.0f;
		ratioFov = tan(fovY);

		// The stepsizes will scale pixels to the range [0.0f, 2.0f] now, so
		// later on it can be scaled to [-1.0f, 1.0f], e.g. in renderScene
		uStepSize = 2.0f / screenWidth;
		vStepSize = 2.0f / screenHeight;

		// Pre-allocate the ray objects
		rays = std::vector<Ray>(screenWidth * screenHeight);
	}

	// For updating the camera plane
	void updateCameraPlane();

	// Used for moving the camera forward, left (backwards and right: use negative values) and up
	void moveCamera(float forwardDistance, float leftDistance, float upDistance);

	// Used for looking around, given dx and dy are changes in the mouse movement
	void updateCameraDirection(int dx, int dy);

	// Sets up and returns all the rays for the camera plane to be used for rendering
	std::vector<Ray>& generateRays();
};

};
