#include "template.h"

#include "tiny_obj_loader.h"
#include "objloader.h"


void resetAccumulator(vec3 *accumulators) {
	for (uint i = 0; i < SCRWIDTH * SCRHEIGHT; ++i) {
		accumulators[i] = vec3(0.0f, 0.0f, 0.0f);
	}
}

void Game::SetupBVHRenderer() {
	// Setup BVH
	printf("Creating BVH...\n");

	if (bvh != NULL) {
		delete bvh;
	}

	bvh = new BVH(scene);
	bvh->constructBVH();
	printf("BVH Creation done!\n");

	// Setup renderer
	if (renderer != NULL) {
		delete renderer;
	}

	renderer = new Renderer(camera, bvh, scene, lights);
}

void Game::Reset() {
	// Clear previous scene
	for (uint i = 0; i < scene.size(); ++i) {
		delete scene[i];
	}

	// TODO: Remove this when switching to path tracing!
	for (uint i = 0; i < lights.size(); ++i) {
		delete lights[i];
	}

	scene.clear();
	lights.clear();
	resetAccumulator(accumulators);
}

void Game::SetupCrappyScene() {
	// Setup ground "plane"
	float3 triangleNormals[3] = {float3(0.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f),
		float3(0.0f, 1.0f, 0.0f)};
	float3 triangleVertices[3] = {float3(0.0f, -1.0f, -100.0f),
		float3(100.0f, -1.0f, 100.0f), float3(-100.0f, -1.0f, 100.0f)};
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE_AND_MIRROR, vec3(0.0f, 1.0f, 0.0f), 0.5)));
		// Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.0f, 1.0f, 0.0f), 0.5)));

	// Setup back "plane"
	triangleNormals[0] = float3(0.0f, 0.0f, -1.0f);
	triangleNormals[1] = float3(0.0f, 0.0f, -1.0f);
	triangleNormals[2] = float3(0.0f, 0.0f, -1.0f);
	triangleVertices[0] = float3(100.0f, -10.0f, 50.0f);
	triangleVertices[1] = float3(0.0f, 100.0f, 50.0f);
	triangleVertices[2] = float3(-100.0f, -10.0f, 50.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.9f, 0.9f, 0.9f))));

	// Setup mirror shit

	// Setup floating pink triangle
	triangleNormals[0] = float3(0.0f, 0.0f, -1.0f);
	triangleNormals[1] = float3(0.0f, 0.0f, -1.0f);
	triangleNormals[2] = float3(0.0f, 0.0f, -1.0f);
	triangleVertices[0] = float3(20.0f, 10.0f, 25.0f);
	triangleVertices[1] = float3(-20.0f, 10.0f, 25.0f);
	triangleVertices[2] = float3(0.0f, 30.0f, 25.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(1.0f, 0.0f, 1.0f))));


	// TODO: Setup lights

	// Load scene from obj
	std::string baseDirectory = "objects/cube/";
	std::string object = baseDirectory + "cube.obj";
	// std::cout << "Object = " << object << ", baseDirectory = " << baseDirectory << std::endl;
	ObjLoader::loadIntoScene(scene, object, baseDirectory, texCache);
}


void Game::SetupBasicScene() {
	// Setup back plane
	float3 triangleNormals[3] = {float3(0.0f, 0.0f, -1.0f),
		float3(0.0f, 0.0f, -1.0f), float3(0.0f, 0.0f, -1.0f)};
	float3 triangleVertices[3] = {float3(-80.0f, -10.0f, 25.0f),
		float3(0.0f, 50.0f, 25.0f), float3(80.0f, -10.0f, 25.0f)};
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.2f, 0.2f, 0.2f), 0.5)));

	// Setup basic light
	triangleNormals[0] = float3(0.0f, 0.0f, 1.0f);
	triangleNormals[1] = float3(0.0f, 0.0f, 1.0f);
	triangleNormals[2] = float3(0.0f, 0.0f, 1.0f);
	triangleVertices[0] = float3(20.0f, 15.0f, 20.0f);
	triangleVertices[1] = float3(20.0f, 15.0f, 20.0f);
	triangleVertices[2] = float3(20.0f, 15.0f, 20.0f);

	// TODO: When converting to path tracer, add light Triangles to scene as well!
	Triangle *temp;
	temp = new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_LIGHT, vec3(1.0f, 1.0f, 1.0f), 500.0f));
	// scene.push_back(temp);
	lights.push_back(temp);

}

void Game::SetupTestScene() {
	// Setup ground plane
	float3 triangleNormals[3] = {float3(0.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f),
		float3(0.0f, 1.0f, 0.0f)};
	float3 triangleVertices[3] = {float3(0.0f, 0.0f, -100.0f),
		float3(100.0f, 0.0f, 100.0f), float3(-100.0f, 0.0f, 100.0f)};
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE_AND_MIRROR, vec3(0.2f, 0.2f, 0.2f), 0.5)));
		// Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.0f, 1.0f, 0.0f), 0.5)));

	// Setup back plane
	triangleNormals[0] = triangleNormals[1] = triangleNormals[2] = float3(0.0f, 0.0f, -1.0f);
	triangleVertices[0] = float3(-80.0f, -10.0f, 25.0f);
	triangleVertices[1] = float3(0.0f, 50.0f, 25.0f);
	triangleVertices[2] = float3(80.0f, -10.0f, 25.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.0f, 0.9f, 0.0f))));

	// Setup back, back plane
	triangleNormals[0] = triangleNormals[1] = triangleNormals[2] = float3(0.0f, 0.0f, 1.0f);
	triangleVertices[0] = float3(-80.0f, -10.0f, -25.0f);
	triangleVertices[1] = float3(0.0f, 50.0f, -25.0f);
	triangleVertices[2] = float3(80.0f, -10.0f, -25.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_MIRROR, vec3(0.5f, 0.5f, 0.5f))));

	// Setup left plane
	triangleNormals[0] = triangleNormals[1] = triangleNormals[2] = float3(1.0f, 0.0f, 0.0f);
	triangleVertices[0] = float3(-30.0f, -10.0f, -50.0f);
	triangleVertices[1] = float3(-30.0f, 50.0f, 0.0f);
	triangleVertices[2] = float3(-30.0f, -10.0f, 50.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.9f, 0.0f, 0.0f))));

	// Setup right plane
	triangleNormals[0] = triangleNormals[1] = triangleNormals[2] = float3(-1.0f, 0.0f, 0.0f);
	triangleVertices[0] = float3(30.0f, -10.0f, -50.0f);
	triangleVertices[1] = float3(30.0f, 50.0f, 0.0f);
	triangleVertices[2] = float3(30.0f, -10.0f, 50.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.0f, 0.0f, 0.9f))));

	// Setup ceiling plane
	triangleNormals[0] = triangleNormals[1] = triangleNormals[2] = float3(0.0f, -1.0f, 0.0f);
	triangleVertices[0] = float3(-100.0f, 25.0f, -50.0f);
	triangleVertices[1] = float3(0.0f, 25.0f, 50.0f);
	triangleVertices[2] = float3(100.0f, 25.0f, -50.0f);
	scene.push_back(new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_DIFFUSE, vec3(0.5f, 0.5f, 0.5f))));


	// Setup/Allocate basic lights part 1
	triangleNormals[0] = float3(0.0f, -1.0f, 0.0f);
	triangleNormals[1] = float3(0.0f, -1.0f, 0.0f);
	triangleNormals[2] = float3(0.0f, -1.0f, 0.0f);
	triangleVertices[0] = float3(-20.0f, 15.0f, 20.0f);
	triangleVertices[1] = float3(20.0f, 15.0f, 20.0f);
	triangleVertices[2] = float3(20.0f, 15.0f, -20.0f);

	// TODO: When converting to path tracer, add light Triangles to scene as well!
	Triangle *temp;
	temp = new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_LIGHT, vec3(1.0f, 1.0f, 1.0f), 500000.0f));
	// scene.push_back(temp);
	lights.push_back(temp);

	// Setup light part 2
	triangleNormals[0] = float3(0.0f, -1.0f, 0.0f);
	triangleNormals[1] = float3(0.0f, -1.0f, 0.0f);
	triangleNormals[2] = float3(0.0f, -1.0f, 0.0f);
	triangleVertices[0] = float3(-20.0f, 15.0f, 20.0f);
	triangleVertices[1] = float3(-20.0f, 15.0f, -20.0f);
	triangleVertices[2] = float3(20.0f, 15.0f, -20.0f);

	// TODO: When converting to path tracer, add light Triangles to scene as well!
	temp = new Triangle(triangleVertices, triangleNormals,
		Material(MATERIAL_TYPE::MATERIAL_LIGHT, vec3(1.0f, 1.0f, 1.0f), 50000000.0f));
	// scene.push_back(temp);
	lights.push_back(temp);
}


// -----------------------------------------------------------
// Initialize the game
// -----------------------------------------------------------
void Game::Init() {
	// Setup texture cache
	texCache = new TextureCache();

	// Setup RNG thing for geometry
	setupGeometryRand();

	// And setup accumulator
	accumulators = new vec3[SCRWIDTH * SCRHEIGHT];
	resetAccumulator(accumulators);

	// Setup camera
	camera = new Camera(float3(0.0f, 1.8f, 0.0f), float3(0.0f, 0.0f, 1.0f),
		1.0f, 30.0f, SCRWIDTH, SCRHEIGHT, 1.0f);

	// Setup/Allocate basic scene...
	// setupCrappyScene();
	SetupBasicScene();


	// Print used keys for controlling the program
	printf("\nKeys for controlling:\n");
	printf("Hold left mouse button to look around\n");
	printf("WASD \t - Move/strafe\n");
	printf("-= \t - Decrease/Increase camera movement speed (plus/minus keys)\n");
	printf("1, 2 \t - Use these to switch between scenes\n");
	printf("i \t - Hold to enable displaying debug fpsCounter\n");
	printf("r \t - Press r to reset the camera to its original position and orientations\n");
	printf("t \t - Press t to switch between comparing and nont-comparing \n");
	printf("ESC \t - Escape key to exit\n");
	printf("\n");


	// And finally create the BVH and renderer
	bvh = NULL;
	renderer = NULL;
	SetupBVHRenderer();
}

void Game::Shutdown() {
	// De-allocate scene...
	for (uint i = 0; i < scene.size(); ++i) {
		// No need to delete lights since they are only references
		delete scene[i];
	}

	// TODO: Remove this when switching to path tracer
	for (uint i = 0; i < lights.size(); ++i) {
		delete lights[i];
	}


	delete camera;
	delete renderer;
	delete bvh;
	delete texCache;
}



// -----------------------------------------------------------
// Input handling
// -----------------------------------------------------------
void Game::HandleInput(float dt) {
	// Handle game input by applying keys that are active
	float distance = dt * camera->movementSpeed;
	float moveForward = 0.0f;
	float moveLeft = 0.0f;
	float moveUp = 0.0f;
	displayDebug = false;

	if (!lookAroundActive) {
		sceneNeedsUpdate = false;
	}

	// Dont update the camera if no keys are being pressed
	if (keysActive.size() == 0) {
		return;
	}

	sceneNeedsUpdate = true;

	for (const int &item : keysActive) {
		switch(item) {
			// For controlling camera movement speed
			case Game::KEY_DECREASE:
				// Make sure its not a negative speed...
				camera->movementSpeed = max(camera->movementSpeed - Game::CAMERA_MOVEMENT_CHANGE, 0.0f);
				sceneNeedsUpdate = false;
				break;
			case Game::KEY_INCREASE:
				camera->movementSpeed = camera->movementSpeed + Game::CAMERA_MOVEMENT_CHANGE;
				sceneNeedsUpdate = false;
				break;

			// For moving forward/back and strafing left/right
			case Game::KEY_W:
				moveForward += distance;
				sceneNeedsUpdate = true;
				break;
			case Game::KEY_A:
				moveLeft += distance;
				sceneNeedsUpdate = true;
				break;
			case Game::KEY_S:
				moveForward -= distance;
				sceneNeedsUpdate = true;
				break;
			case Game::KEY_D:
				moveLeft -= distance;
				sceneNeedsUpdate = true;
				break;

			// For moving up/down
			case Game::KEY_SPACE:
				moveUp += distance;
				sceneNeedsUpdate = true;
				break;
			case Game::KEY_C:
			case Game::KEY_LSHIFT:
				moveUp -= distance;
				sceneNeedsUpdate = true;
				break;

			// For loading scenes
			case Game::KEY_1:
				Reset();
				SetupCrappyScene();
				SetupBVHRenderer();
				sceneNeedsUpdate = true;
				break;

			case Game::KEY_2:
				Reset();
				SetupTestScene();
				SetupBVHRenderer();
				sceneNeedsUpdate = true;
				break;

			// For resetting camera position
			case Game::KEY_R:
				camera->position = float3(0.0f, 0.0f, -10.0f);
				camera->direction = float3(0.0f, 0.0f, 1.0f);
				camera->updateCameraPlane();
				sceneNeedsUpdate = true;
				break;

			// For debug shit
			case Game::KEY_DEBUG:
				displayDebug = true;
				sceneNeedsUpdate = true;
				break;

			default:
				sceneNeedsUpdate = lookAroundActive;
				break;
		}
	}

	// Move the camera only if we have to
	if (keysActive.size() == 0 || (moveForward == 0.0f && moveLeft == 0.0f && moveUp == 0.0f)) {
		return;
	}

	camera->moveCamera(moveForward, moveLeft, moveUp);
	sceneNeedsUpdate = true;
}

void Game::KeyDown(int key) {
	// Add the keys to the keysActive list, but dont do anything if the key was already pressed
	keysActive.emplace(key);
}

void Game::KeyUp(int key) {
	// Remove it from the keysActive list, if it is present there
	// printf("KeyUp: %d\n", key);
	keysActive.erase(key);
}



void Game::MouseUp(int button) {
	if (button == Game::LEFT_MOUSE_BUTTON) {
		lookAroundActive = false;
		sceneNeedsUpdate = false;
		// printf("Mouse up!\n");
	}
}

void Game::MouseDown(int button) {
	if (button == Game::LEFT_MOUSE_BUTTON) {
		lookAroundActive = true;
		firstTime = true;
		// printf("Mouse down!\n");
	}
}

void Game::MouseMove(int x, int y) {
	if (lookAroundActive) {
		showColorX = showColorY = -1;

		// Update the previous x and y with the current x and y if this is the first time
		// that we are looking around, or if we have just looked around. This is to prevent
		// the enormous increase in dx and dy if we have already moved the mouse...
		if (firstTime) {
			previousX = x;
			previousY = y;
			firstTime = false;
			return;
		}

		// Only update rendering if we actually move the mouse
		int dx = x - previousX, dy = y - previousY;

		if (dx == 0 && dy == 0) {
			return;
		}

		camera->updateCameraDirection(dx, dy);
		previousX = x, previousY = y;
		sceneNeedsUpdate = true;
	} else {
		// Track mouse position so we can show the pixel color at that position
		showColorX = x;
		showColorY = y;
	}
}

void Game::ShowDetails(float dt) {
	Pixel *buffer = screen->GetBuffer();

	// Check if it's near the edges, since its likely that we lost application focus in that case
	// TODO: Fix this by detecting application focus properly (aka, fuck this for now)
	bool showPixelRGB = (showColorX >= 1 && showColorY >= 1 &&
		showColorX < SCRWIDTH - 1 && showColorY < SCRHEIGHT - 1);

	// Create a slightly black rectangle to show the details more clearly
	uint yMax = (showPixelRGB) ? 43 : 38;

	for (uint y = 0; y < yMax; ++y) {
		uint xMax = 195;

		if (showPixelRGB && y >= 31) {
			xMax = 340;
		}

		for (uint x = 0; x < xMax; ++x) {
			Pixel previous = buffer[x + y * screen->GetPitch()];
			screen->Plot(x, y, ScaleColor(previous, 60));
		}
	}

	// Show an fps counter in the top left corner...
	std::string fpsCounter = "FPS: ";

	if (dt != 0.0f) {
		// Round down to nearest .5 fps, and skip trailing zeros
		fpsCounter += std::to_string(round(10.0f * 1.0f / dt) / 10.0f);
		fpsCounter.erase(fpsCounter.find_last_not_of('0') + 1, std::string::npos);
	} else {
		fpsCounter += "NAN";
	}

	screen->Print(fpsCounter.c_str(), 2, 2, 0xffffff);

	// And also show the time spent converging
	std::string converge = "Converging time (s): ";
	converge += std::to_string(round(10.0f * convergingTime) / 10.0f);
	converge.erase(converge.find_last_not_of('0') + 1, std::string::npos);
	screen->Print(converge.c_str(), 2, 10, 0xffffff);

	// And also show the sum of all colors
	std::string colorSum = "Sum of all colors: ";
	colorSum += std::to_string(renderer->sumOfAllColors);
	screen->Print(colorSum.c_str(), 2, 18, 0xffffff);

	// And show the number of samples used so far
	std::string numSamples = "Num samples used: ";
	numSamples += std::to_string(numSamplesUsed);
	screen->Print(numSamples.c_str(), 2, 26, 0xffffff);

	// And optionally show pixel RGB color based on mouse position
	if (showPixelRGB) {
		// Get RGB color at that pixel
		Pixel pixel = buffer[showColorX + showColorY * screen->GetPitch()];
		vec3 rgb = PixelToFloatColor(pixel);
		std::string rgbShower = "Pixel (" + std::to_string(showColorX) + ", " +
			std::to_string(showColorY) + "): ";

		// Create string rgbShower like: "R: 0.1, G: 0.9, B: 1.0"
		// We don't round these values because thats useless
		rgbShower += "R: " + std::to_string(rgb.x) + ", ";
		rgbShower += "G: " + std::to_string(rgb.y) + ", ";
		rgbShower += "B: " + std::to_string(rgb.z);
		screen->Print(rgbShower.c_str(), 2, 34, 0xffffff);
	}
}

// -----------------------------------------------------------
// Main game tick function
// -----------------------------------------------------------
void Game::Tick(float dt) {
	// Update our position using input
	HandleInput(dt);
	BVH::DISPLAY_DEBUG = displayDebug;

	// Do some pre-render stuff
	if (sceneNeedsUpdate || firstTick) {
		firstTick = false;

		if (!displayDebug) {
			frameNr = 0;
			convergingTime = 0.0f;
			numSamplesUsed = 0;
			resetAccumulator(accumulators);
		}

		if (displayDebug && dt != 0.0f) {
			resetAccumulator(accumulators);
			screen->Clear(0);
			printf("FPS: %.1f, Camera position (%.2f, %.2f, %.2f), Camera direction (%.2f, %.2f, %.2f), sumOfAllColors = %llu\n",
				1.0f / dt, camera->position.x, camera->position.y, camera->position.z,
				camera->direction.x, camera->direction.y, camera->direction.z,
				renderer->sumOfAllColors);

			// Reset firstTick to true again to "remove" debug crap when letting
			// go of the debug button
			firstTick = true;
		}
	} else {
		convergingTime += dt;
		numSamplesUsed += renderer->SPP;
	}

	// Then do the actual rendering, or let the path tracer converge
	screen->Clear(0);
	resetAccumulator(accumulators);
	renderer->renderScene(screen, displayDebug, frameNr, accumulators);
	ShowDetails(dt);
	++frameNr;
}
