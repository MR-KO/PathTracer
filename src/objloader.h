#pragma once

namespace Tmpl8 {

// This class is purely meant to be used statically...
class ObjLoader {
public:
	// Loads the object from the .obj file into the scene, meterialDir is
	// optional, and should hold the directory of the .mtl file(s). The texture
	// cache is used to load textures
	static void loadIntoScene(std::vector<Triangle *> &scene, std::string &objectFile,
		std::string &materialDir, TextureCache *texCache);

private:
	ObjLoader() { }

	// Converts .obj material type to our own material types
	static Material convertObjMaterial(std::vector<tinyobj::material_t> &materials,
		int materialId, std::string materialDir, TextureCache *texCache, bool hasTexture);
};

};
