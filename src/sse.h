#pragma once

namespace Tmpl8 {

typedef __m128 quadfloat;
typedef __m128i quadint;


inline quadfloat getQuadfloat(float a, float b, float c, float d) {
	return _mm_set_ps(a, b, c, d);
}

inline quadint getQuadint(int a, int b, int c, int d) {
	return _mm_set_epi32(a, b, c, d);
}



inline quadfloat addf(quadfloat a, quadfloat b) {
	return _mm_add_ps(a, b);
}

inline quadfloat subf(quadfloat a, quadfloat b) {
	return _mm_sub_ps(a, b);
}

inline quadfloat mulf(quadfloat a, quadfloat b) {
	return _mm_mul_ps(a, b);
}

inline quadfloat divf(quadfloat a, quadfloat b) {
	return _mm_div_ps(a, b);
}



inline quadint addi(quadint a, quadint b) {
	return _mm_add_epi32(a, b);
}

inline quadint subi(quadint a, quadint b) {
	return _mm_sub_epi32(a, b);
}

};